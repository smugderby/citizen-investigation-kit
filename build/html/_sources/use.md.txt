

# USING THE KIT

## Basic Concepts

### INVESTIGATION

What is it, what counts as an investigation
Why do you investigate,what makes you start and what made other people start
How do you investigate
what it needs as 'ingredients'
what is the process, trying not to formalize it at all and leave room for creative work but still show that there is a process even if that is shaped and reshaped along the way based on lots of factors and findings (or lack of findings)
how you document your process and findings

Sources: https://void.tacticaltech.org/p/investigationkit_designing_your_investigation
     https://void.tacticaltech.org/p/investigationkit_chainofcustody_lawstuff
 https://void.tacticaltech.org/p/investigationkit_findinginformation
https://void.tacticaltech.org/p/investigationkit_analyzing+interpreting
https://void.tacticaltech.org/p/investigationkit_cleaningandmanagingdata


### EVIDENCE

(These below can be sub-sections under Evidence or some can go separately or in a different arrangement. I would also use a lot of the content from this pad here: https://void.tacticaltech.org/p/investigationkit_findinginformation

 https://void.tacticaltech.org/p/investigationkit_concepts

 What is Evidence
what is the difference between evidence, information, statements etc.
what is not evidence
how even lack of evidence constitutes evidence etc.
Sources of Evidence
Evidence you are given
Evidence that is open
Evidence you request
Evidence you provide (create?)
....
Formats/forms of Evidence
Image
Sound
Written
digital
analoque
....
Evidence Collection and Management
Safeguarding Evidence
Using Evidence

Sources: https://void.tacticaltech.org/p/investigationkit_verification, others


### DOCUMENTATION

documenting your investigation, method and entire process
referencing information and findings
preserving findings for later use and sharing
...


### VERIFICATION AND FACTCHECKING

what it means
why it matters
when it should happen
who should do it
how to do it - general approach
how to document your verification process (to provefindings)


### INVESTIGATION ENVIRONMENTS

 Digital research
 Desk resrach
 Field work
 Low-tech investigations

Sources: https://void.tacticaltech.org/p/investigationkit_lowtech


### INVESTIGATION TECHNIQUE

what they mean; how they define the data you find and collect
how they relate to your investigation questions and purpose
introduce the techniques we are focusing on here


### INVESTIGATION TYPE

what this means, what you need to know when you start, what questions you ask
how you identify and define your purpose, actors, necessary evidence and sources of information to begin with
introduce the types we are focusing on here


## HOW YOU INVESTIGATE

### FIELD RESEARCH
Sources:  https://void.tacticaltech.org/p/investigationkit_fieldresearchprinciples


### INTERVIEWS

- 1. Conducting Interviews
- 2. Source Management
Sources: https://void.tacticaltech.org/p/investigationkit_sourcemanagement


### RE/SEARCHING INTERNET WEBSITES

- 1. Google Dorking
- 2. Internet Archives

Sources: ETI Website
https://void.tacticaltech.org/p/investigationkit_webinv


### INVESTIGATING ON SOCIAL MEDIA

- 1. Find sources and eyewitnesses
- 2. How to find out more about specific topics/subjects
- 3. Verify Social Media Content
- 4. How to save and organise the list of findings
- 5. How to save posts, photos and videos from social networks

Sources:https://void.tacticaltech.org/p/investigationkit_social_media


### INVESTIGATING WITH IMAGES



### INVESTIGATING WITH MAPS AND GEOLOCATION

- 1.Geo data
- 2.Map formats
- 3.Mapping tools
- 4.Identifying places from photos
- 5.Processing/converting/collating geo data
- 6.Identification of features in satellite imagery
- 7.Observing change/ space and time
Sources:  https://void.tacticaltech.org/p/investigationkit_mapping


### INVESTIGATING WITH SOUND

- 1.Sound Basics - what you need to know
- 2.Investigating with available sound resources - collecting evidence from existing recordings
- 3.Creating sound resources for investigation - recording, capturing evidence, documenting the process
- 4.Voice Matching?
- 5.Investigations based on Sound (cases - can be incorporated throiughout the text above where it applies)

Sources:  https://void.tacticaltech.org/p/investigationkit_sound


### VERIFICATION TECHNIQUES
...
Sources: https://void.tacticaltech.org/p/investigationkit_verification


## WHAT YOU INVESTIGATE

### COMPANIES
(incl, profiling companies and related people, mapping ownership trees, reading reports and financial statements, identifying red flags etc.)
Sources: https://void.tacticaltech.org/p/investigationkit_fininv / Updated structure: https://void.tacticaltech.org/p/investigationkit_corporate

### PUBLIC FUNDS
(incl. public contracting, large infrastructure projects, preferrential agreements, red flags etc.)
Sources: https://void.tacticaltech.org/p/investigationkit_fininv / Updated structure: https://void.tacticaltech.org/p/investigationkit_corporate

### CORPORATE - POLITICAL CONNECTIONS
(incl. lobbying, revolving door, state capture etc.)
Sources: https://void.tacticaltech.org/p/investigationkit_fininv / Updated structure: https://void.tacticaltech.org/p/investigationkit_corporate

### PUBLIC STATEMENTS
(incl. identifying statements, contexts of statements, verification of claims and data, mapping data sources, revealing manipulation of data etc.)
Sources:https://void.tacticaltech.org/p/investigationkit_social_media
https://void.tacticaltech.org/p/investigationkit_verification

### SUPPLY CHAINS
(incl. tracking product sources, production processes, actors, red flags etc.)
Sources: https://void.tacticaltech.org/p/investigationkit_supplychains

### ELECTION INFLUENCE
(incl. actors and mechanisms involved in election campaigns, methods of influence, verification of campaign adds, use of voters information for targeted campaigns, businesses-candidates connections, red flags etc. --- pending topic)
Sources: TTC Data and Elections



## GENERAL CONSIDERATIONS
(not sure yet if we need this separately or we personalize them for each separate technique and type of investigation. We may need a general one on Safety)

### ETHICS
Sources: https://void.tacticaltech.org/p/investigationkit_ethics


### SAFETY
Sources: https://void.tacticaltech.org/p/investigationkit_risk

### LEGAL

### COLLABORATION
Sources:  https://void.tacticaltech.org/p/investigationkit_collaboration



## WHERE NEXT

An open ending to this kit series (not a boring conclusion):
Talking about how you can use your evidence and investigations
Providing an overview of what else is possible to achieve from here on


PARKED:

https://void.tacticaltech.org/p/investigationkit_impact
https://void.tacticaltech.org/p/invesigationkit_hria
https://void.tacticaltech.org/p/investigationkit_best_practices_documentation
https://void.tacticaltech.org/p/investigationkit_post-its2
