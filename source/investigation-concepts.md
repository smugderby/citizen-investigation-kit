#Investigation Concepts

## What is evidence - what evidence should be

It is impossible to recreate history- even with the most complete, agreed-upon narrative, there is no way to PROVE what happened- or, in another sense, what is happening. When you are trying to present a narrative, either of a historical truth or a hidden truth or a current reality that is unspoken or unseen, you need to prove it to those you're sharing it with; in fact, you need to prove it to yourself. Otherwise you're just telling a story, that may or may not be true.

The past leaves behind residue: dust, footprints, documents, videos, audio recording, witnesses, scent, paperwork, presence or absence of something that wasn't or was there before. Finding and documenting this residue is broadly what collecting evidence means. This residue of events, relationships, transactions or places past or current is what "proves" your story to have a basis in reality. Fundamentally, an investigation is a process of organized evidence collection, that seeks to be as close to the truth of past reality as possible.

What is evidence then? For the purposes of this kit, we will use the generic meaning of evidence: some kind of information that is material to the question, problem, person or process you're investigating. There are legal definitions of evidence that can be much more strict, but this more generic meaning will serve more diverse investigations.

Think of evidence as the bedrock upon which you build the foundation of your investigation: collect enough of it, and the intelligence you build will be well supported and closer to the truth. With patchy, unproven, false or limited evidence, any conclusions you try to make from your information will be incomplete at best, and completely wrong at worst. You want an impenetrable foundation.

So: evidence is information that exists out there in the world, left by the natural progression of time. Collect enough, and you can start to see what story it is telling you. You shouldn't just ensure you have ENOUGH evidence: you must also ensure your evidence is GOOD. [Quantity will NEVER outperform quantity.]

## What is good evidence?

Good evidence can be shared with other people.
Good evidence can be documented and preserved.
Good evidence is firsthand.
GOOD EVIDENCE CAN BE VERIFIED OR CROSS-CONFIRMED
Good evidence has origins that can be proven by a third party.
Good evidence can be confirmed by a third party.
Good evidence is TIMELY to the event in question: created or documented close to when the event occured.
[Good evidence has an auditable chain of custody.]
Good evidence is uncorrupted, and is kept SAFE from corruption or tampering
[Good evidence has metadata]
Good evidence connects to other pieces of evidence: good evidence links information together
Good evidence will direct you to places where your information is weak or incomplete
[REALLY GOOD evidence speaks for itself.] [lol]
Good evidence doesn't put its human sources at risk: or that risk can be managed by the investigator
[Good evidence is accessible to people without education, resources or training]
Good evidence may challenge your preconceived idea or narrative
Good evidence may DISPROVE what you think is true

Not every piece of evidence you discover will meet all the above conditions. That's absolutely fine, and to be expected. But: some of the evidence you find will meet a lot of them, and good evidence should take priority over the not-as-good, even if isn't telling you what you want to hear.

The worst historians, and least-reputable politicians, will insist on a certain story or narrative and will selectively present information that supports the story they want to tell. They will rely on their audience to believe without checking for themselves what information actually exists to prove or disprove their stories. After all, investigation is hard work. Creating an impenetrable foundation doesn't just mean building up a pile of evidence to support your desired narrative: it also means sifting through evidence that may completely destroy it, and considering and presenting that as well. Any honest investigator will acknowledge that their conclusions are based upon what they are able to find. An honest investigator will take a balanced look at the BEST evidence possible, and a lot of it, before making any conclusions. An honest investigation is transparent and auditable, from start to finish.

## Process Documentation

[ADAPTED FROM investigationkit_chainofcustody_lawstuff]

The evidence collection, lead-following, interviewing, modeling, mapping, planning and thinking that form your investigation will be more useful to you, and more supportive of your conclusions, if you document your investigative process. The primary goal of documenting your process is to create a contemporaneous, auditable record of your investigation. This is a habit of "investigative hygiene" - a constant maintenance of certain practices to best ensure your investigation is run correctly. Think of it like physical hygiene; while you may not always feel like brushing your teeth or changing your bedsheets, hopefully you do it regularly to maintain your health. You want to have a healthy investigative practice- this will make you more effective as an investigator, and less likely to struggle with common mistakes when starting out. Having this record isn't just for outsiders: it can help you as you progress to remember prior conclusions, where you got leads, evidence that seemed useless before but now seems important, things you forgot to follow up on, or meant to do, or need to ask someone else to help with.

Documenting your investigative steps and the reasoning behind them is not intuitive. Many people or groups will assume they'll remember how a process unfolded, or that it doesn't matter anyway, as long as the evidence or story is uncovered. However being able to state with confidence why you may have pursued one source over another, or where you got an address, or what time a conversation took place, for example - makes you a much more valuable resource if you will ever testify in court as to your findings, or if law enforcement should want to retrace your work, verify your information or use your evidence.    will lend more credibility to your work and will help others understand your process.

Process documentation habits are useful if you ever think of speaking to law enforcement; or should your work be interrupted, by your private life, professional life, imprisonment, political instability or death, your work will be better able to be interpreted by anyone reviewing it, taking on the rest of the investigation, or seeking to use the evidence you managed to uncover. These habits are also useful if you end up collaborating with other partners, to catch them up to speed on how things had been unfolding before their involvement. Long story short: anyone who has access to your investigation log and your evidence should be able to understand exactly what you uncovered and how.

[Different documentation structures - ex. Markdown, physical notetaking, other documents etc]

## How-To

+if you are interviewing a witness or other human source, or meeting with other investigators, take notes. Record the date, location and beginning/ending times of the meeting, and the participants. Note what was discussed and if any next meeting may be held with the same people or witness ("The witness and I decided we would speak again over the phone on Thursday at 14:00 after they review their records..." etc).

+If you're creating a list of contacts, list not only their name and contact information, but also how you came to know them, each time you have met, and a summary of the sort of information they have offered you. It is best practice if you have sensitive (i.e. at-risk) sources to give them an alias in your notes and working documents.

+If you have a list of businesses or phone numbers to research, for example, make sure you note where you first encountered the business name or phone number.

+Keep a log of investigative steps you have taken and the outcome, with dates and signature; for example, "On January 3rd, 2018, I and my research partner captured satellite images using [insert tool] and documented the location of the company's construction sites." or "On March 14th, 2018, I tried calling different public phone numbers associated with the company to see if I could get records, but I was diverted to a voicemail machine."

+Once you have what you feel is enough information to begin, it can be helpful to create a rough narrative outline of what you have figured out. This can serve as a preservation of different stages of the investigation, and will help you to see what information may be missing, if there are next steps you hadn't identified, and if certain information needs to be verified or refined.

+You should create a log of all evidence (physical and digital!) you have collected, with dates, origins, storage location and security information. This should also include a description of the state of the evidence and who collected it and has interacted with it.


## Glossary

Evidence
Chain of Custody
Auditable
Metadata
Third Party


## Parking Lot

diversification of investigation types. I'm concerned that the writing is too focused on corruption/financial/investigative journalism and doesn't address other populations we want to reach like artists and nonprofessionals
is the language accessible enough for ESL readers and/or translation
